﻿using System;

namespace otus.solid
{
    class NumericGenerator : IGenerator<int>
    {
        public int GetValue(int min, int max)
            => new Random().Next(min, max);
    }
}
