﻿namespace otus.solid
{
    class GuessTheNumberGame
    {
        private readonly IConfiguration _configuration;
        private readonly IGenerator<int> _generator;
        private readonly IInput<int> _input;
        private readonly IOutput _output;
        private readonly IComparer<int> _comparer;
        bool _hasWonTheGame;
        public GuessTheNumberGame(IConfiguration configuration,
                                  IGenerator<int> generator,
                                  IInput<int> input,
                                  IOutput output,
                                  IComparer<int> comparer)
        {
            _configuration = configuration;
            _generator = generator;
            _input = input;
            _output = output;
            _comparer = comparer;
        }
        public void Start()
        {
            int correctValue = _generator.GetValue(_configuration.MinValue, _configuration.MaxValue);
            _comparer.OnMatch += GuessedTheNumber;
            _output.Output($"You have {_configuration.Attempts} attempts to guess the number. Good luck!");
            for (int i = 0; i < _configuration.Attempts; i++)
            {
                _output.Output("Enter numeric value:");
                int attemptValue = _input.GetValue();
                string result = _comparer.Compare(attemptValue, correctValue);
                if (_hasWonTheGame) return;
                _output.Output(result);
                _output.Output($"You have {_configuration.Attempts - (i + 1)} attempts left");
            }
            _output.Output($"Unfortunately your luck has ran out. The correct number was: {correctValue}");
        }

        void GuessedTheNumber(string message)
        {
            _output.Output(message);
            _comparer.OnMatch -= GuessedTheNumber;
            _hasWonTheGame = true;
        }
    }
}
