﻿namespace otus.solid
{
    interface IOutput
    {
        void Output(string message);
    }
}
