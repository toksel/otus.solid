﻿namespace otus.solid
{
    interface IGenerator<T>
    {
        T GetValue(T min, T max);
    }
}
