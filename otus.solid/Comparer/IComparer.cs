﻿using System;

namespace otus.solid
{
    interface IComparer<T>
    {
        event Action<string> OnMatch;
        string Compare(T input, T value);
    }
}
