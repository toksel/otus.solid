﻿using System;

namespace otus.solid
{
    class ConsoleOutput : IOutput
    {
        public void Output(string message)
            => Console.WriteLine(message);
    }
}
