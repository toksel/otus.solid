﻿namespace otus.solid
{
    interface IInput<T>
    {
        T GetValue();
    }
}
