﻿using System;

namespace otus.solid
{
    class NumericComparer : IComparer<int>
    {
        public event Action<string> OnMatch;

        public string Compare(int input, int value)
        {
            if (input == value)
            {
                OnMatch?.Invoke("You have won the game. My sincere congratulations!");
                return null;
            }
            return input > value ? "Your number is greater" : "Your number is lower";
        }
    }
}
