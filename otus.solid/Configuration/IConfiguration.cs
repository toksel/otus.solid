﻿namespace otus.solid
{
    interface IConfiguration
    {
        public int MinValue { get; }
        public int MaxValue { get; }
        public int Attempts { get; }
    }
}
