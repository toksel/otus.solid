﻿namespace otus.solid
{
    public class Configuration : IConfiguration
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int Attempts { get; set; }
    }
}
