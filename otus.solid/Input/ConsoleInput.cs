﻿using System;

namespace otus.solid
{
    class ConsoleInput : IInput<int>
    {
        public int GetValue()
        {
            int result;
            string value;
            do
            {
                value = Console.ReadLine();
            }
            while (!int.TryParse(value, out result));
            return result;
        }
    }
}
