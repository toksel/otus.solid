﻿using Microsoft.Extensions.Configuration;

namespace otus.solid
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder().AddJsonFile("Configuration.json", false).Build();
            var settings = builder.GetSection("AppSettings").Get<Configuration>();


            GuessTheNumberGame game = new(settings,
                                          new NumericGenerator(),
                                          new ConsoleInput(),
                                          new ConsoleOutput(),
                                          new ChildNumertiComparer());
            game.Start();
        }
    }
}
